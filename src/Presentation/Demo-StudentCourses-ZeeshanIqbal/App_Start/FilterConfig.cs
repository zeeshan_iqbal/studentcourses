﻿using System.Web;
using System.Web.Mvc;

namespace Demo_StudentCourses_ZeeshanIqbal
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
