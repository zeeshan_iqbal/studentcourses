﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Demo_StudentCourses_ZeeshanIqbal.Startup))]
namespace Demo_StudentCourses_ZeeshanIqbal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
